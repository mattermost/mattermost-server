package cern

import (
	"net/http"
	"strings"

	"github.com/mattermost/mattermost/server/public/model"
	"github.com/mattermost/mattermost/server/public/shared/mlog"
	cern_interfaces "github.com/mattermost/mattermost/server/v8/cern/interfaces"
	"github.com/mattermost/mattermost/server/v8/channels/app"
)

type CERNAccessInterface struct {
	srv *app.Server
}

func init() {
	app.RegisterCERNAccessInterface(func(srv *app.Server) cern_interfaces.CERNAccessInterface {
		mlog.Info("CERN/Access: Registering interface")
		return &CERNAccessInterface{
			srv: srv,
		}
	})
}

func doesUserHaveCERNAccount(user *model.User) bool {
	if user.AuthService != "gitlab" {
		return false
	}
	if user.AuthData == nil {
		// not sure if needed, but better than a panic
		return false
	}
	return strings.HasPrefix(*user.AuthData, "cern:")
}

func (ca *CERNAccessInterface) App() *app.App {
	return app.New(app.ServerConnector(ca.srv.Channels()))
}

func (ca *CERNAccessInterface) ConsiderUserAsCERNEmail(user *model.User, allowedDomains []string, isEmailAddressAllowed cern_interfaces.IsEmailAddressAllowedFunc) bool {
	return doesUserHaveCERNAccount(user) && isEmailAddressAllowed("dummy@cern.ch", allowedDomains)
}

func (ca *CERNAccessInterface) RestrictTeamToCERN(team *model.Team) {
	team.AllowedDomains = "cern.ch"
}

func (ca *CERNAccessInterface) CheckTeamCreatorCERN(userId string) *model.AppError {
	user, err := ca.App().GetUser(userId)
	if err != nil {
		return err
	}
	if !doesUserHaveCERNAccount(user) && !user.IsSystemAdmin() {
		return model.NewAppError("createTeam", "Only users with a CERN account may create teams", nil, ".", http.StatusForbidden)
	}
	return nil
}
