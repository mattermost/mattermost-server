package cern_interfaces

import (
	"github.com/mattermost/mattermost/server/public/model"
)

type IsEmailAddressAllowedFunc func(string, []string) bool

type CERNAccessInterface interface {
	ConsiderUserAsCERNEmail(user *model.User, allowedDomains []string, isEmailAddressAllowed IsEmailAddressAllowedFunc) bool
	RestrictTeamToCERN(team *model.Team)
	CheckTeamCreatorCERN(userId string) *model.AppError
}
