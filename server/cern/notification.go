package cern

import (
	"net/http"
	"strings"

	"github.com/mattermost/mattermost/server/public/model"
	"github.com/mattermost/mattermost/server/public/shared/i18n"
	"github.com/mattermost/mattermost/server/public/shared/mlog"
	"github.com/mattermost/mattermost/server/public/shared/request"
	"github.com/mattermost/mattermost/server/v8/channels/app"
	"github.com/mattermost/mattermost/server/v8/einterfaces"
)

type NotificationInterface struct {
	srv *app.Server
	app *app.App
}

func init() {
	app.RegisterNotificationInterface(func(app *app.App) einterfaces.NotificationInterface {
		mlog.Info("CERN/Notification: Registering custom notification interface")
		return &NotificationInterface{
			srv: app.Srv(),
			app: app,
		}
	})
}

func getPushNotificationMessage(postMessage string, hasFiles bool, senderName string, channelType model.ChannelType, userLocale i18n.TranslateFunc) string {
	// this is similar to the namesake function in the app itself, but simplified since we always want full message contents and we don't have access to the original function here
	if postMessage == "" && hasFiles {
		if channelType == model.ChannelTypeDirect {
			return strings.Trim(userLocale("api.post.send_notifications_and_forget.push_image_only"), " ")
		}
		return senderName + userLocale("api.post.send_notifications_and_forget.push_image_only")
	}

	if channelType == model.ChannelTypeDirect {
		return model.ClearMentionTags(postMessage)
	}
	return senderName + ": " + model.ClearMentionTags(postMessage)
}

func canViewPost(c request.CTX, app *app.App, user *model.User, channel *model.Channel, post *model.Post) bool {
	if app.HasPermissionToChannel(c, user.Id, channel.Id, model.PermissionReadChannel) {
		return true
	}
	if channel.Type == model.ChannelTypeOpen && app.HasPermissionToTeam(c, user.Id, channel.TeamId, model.PermissionReadPublicChannel) {
		return true
	}
	return false
}

func (n *NotificationInterface) GetNotificationMessage(c request.CTX, ack *model.PushNotificationAck, userID string) (*model.PushNotification, *model.AppError) {
	a := n.app
	cfg := a.Config()

	post, err := a.GetSinglePost(c, ack.PostId, false)
	if err != nil {
		return nil, err
	}
	channel, err := a.GetChannel(request.EmptyContext(a.Log()), post.ChannelId)
	if err != nil {
		return nil, err
	}
	user, err := a.GetUser(post.UserId)
	if err != nil {
		return nil, err
	}
	notificationUser, err := a.GetUser(userID)
	if err != nil {
		return nil, err
	}

	// make sure the user can actually see the post
	if !canViewPost(c, a, notificationUser, channel, post) {
		mlog.Error("User tried to get notification message without access", mlog.String("user_id", userID), mlog.String("post_id", post.Id))
		return nil, model.NewAppError("cern", "notification.no_access", nil, "User tried to get notification message without access", http.StatusForbidden)
	}

	msg := &model.PushNotification{
		Category:    model.CategoryCanReply,
		Version:     model.PushMessageV2,
		Type:        model.PushTypeMessage,
		TeamId:      channel.TeamId,
		ChannelId:   channel.Id,
		PostId:      post.Id,
		RootId:      post.RootId,
		SenderId:    post.UserId,
		ChannelName: channel.DisplayName,
		IsIdLoaded:  true,
	}

	// based on the logic in buildFullPushNotificationMessage
	userNameFormat := a.GetNotificationNameFormat(notificationUser)
	msg.SenderName = user.GetDisplayNameWithPrefix(userNameFormat, "@")
	if ou, ok := post.GetProp("override_username").(string); ok && *cfg.ServiceSettings.EnablePostUsernameOverride {
		msg.OverrideUsername = ou
		msg.SenderName = ou
	}
	if oi, ok := post.GetProp("override_icon_url").(string); ok && *cfg.ServiceSettings.EnablePostIconOverride {
		msg.OverrideIconURL = oi
	}
	if channel.Type == model.ChannelTypeDirect {
		msg.ChannelName = msg.SenderName
	}

	if fw, ok := post.GetProp("from_webhook").(string); ok {
		msg.FromWebhook = fw
	}

	postMessage := post.Message
	for _, attachment := range post.Attachments() {
		if attachment.Fallback != "" {
			postMessage += "\n" + attachment.Fallback
		}
	}

	userLocale := i18n.GetUserTranslations(notificationUser.Locale)
	hasFiles := post.FileIds != nil && len(post.FileIds) > 0
	msg.Message = getPushNotificationMessage(postMessage, hasFiles, msg.SenderName, channel.Type, userLocale)
	return msg, nil
}

func (n *NotificationInterface) CheckLicense() *model.AppError {
	// this is custom code which does not require a license :)
	return nil
}
