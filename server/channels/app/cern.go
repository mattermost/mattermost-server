package app

import (
	cern_interfaces "github.com/mattermost/mattermost/server/v8/cern/interfaces"
)

var cernAccessInterface func(*Server) cern_interfaces.CERNAccessInterface

func RegisterCERNAccessInterface(f func(*Server) cern_interfaces.CERNAccessInterface) {
	cernAccessInterface = f
}

func (s *Server) initCERN() {
	if cernAccessInterface != nil {
		s.CERNAccess = cernAccessInterface(s)
	}
}
