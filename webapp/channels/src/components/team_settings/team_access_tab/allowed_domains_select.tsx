// Copyright (c) 2015-present Mattermost, Inc. All Rights Reserved.
// See LICENSE.txt for license information.

import React, {useCallback, useState} from 'react';
import {FormattedMessage, useIntl} from 'react-intl';

import SelectTextInput, {type SelectTextInputOption} from 'components/common/select_text_input/select_text_input';
import CheckboxSettingItem from 'components/widgets/modals/components/checkbox_setting_item';
import {type SaveChangesPanelState} from 'components/widgets/modals/components/save_changes_panel';

type Props = {
    allowedDomains: string[];
    setAllowedDomains: (domains: string[]) => void;
    setHasChanges: (hasChanges: boolean) => void;
    setSaveChangesPanelState: (state: SaveChangesPanelState) => void;
}

const AllowedDomainsSelect = ({allowedDomains, setAllowedDomains, setHasChanges, setSaveChangesPanelState}: Props) => {
    const [showAllowedDomains, setShowAllowedDomains] = useState<boolean>(allowedDomains.length > 0);
    const {formatMessage} = useIntl();

    const handleEnableAllowedDomains = useCallback((enabled: boolean) => {
        setShowAllowedDomains(enabled);
        if (!enabled) {
            setHasChanges(true);
            setSaveChangesPanelState('editing');
            setAllowedDomains([]);
        }
    }, [setAllowedDomains, setHasChanges, setSaveChangesPanelState]);

    const updateAllowedDomains = useCallback((domain: string) => {
        setHasChanges(true);
        setSaveChangesPanelState('editing');
        setAllowedDomains([...allowedDomains, domain]);
    }, [allowedDomains, setAllowedDomains, setHasChanges, setSaveChangesPanelState]);

    const handleOnChangeDomains = useCallback((allowedDomainsOptions?: SelectTextInputOption[] | null) => {
        setHasChanges(true);
        setSaveChangesPanelState('editing');
        setAllowedDomains(allowedDomainsOptions?.map((domain) => domain.value) || []);
    }, [setAllowedDomains, setHasChanges, setSaveChangesPanelState]);

    const hasCERNRestriction = allowedDomains.includes('cern.ch');
    const hasOnlyCERNRestriction = allowedDomains.length === 1 && hasCERNRestriction;
    const setCERNRestriction = () => {
        updateAllowedDomains('cern.ch');
    };

    return (
        <>
            <CheckboxSettingItem
                inputFieldTitle={
                    <FormattedMessage
                        id='general_tab.allowedDomains'
                        defaultMessage='Allow only users with a specific email domain to join this team'
                    />
                }
                data-testid='allowedDomainsCheckbox'
                className='access-allowed-domains-section'
                title={formatMessage({
                    id: 'general_tab.AllowedDomainsTitle',
                    defaultMessage: 'Users with a specific email domain',
                })}
                description={
                    <span>
                        {formatMessage({
                            id: 'general_tab.AllowedDomainsInfo',
                            defaultMessage: 'When enabled, users can only join the team if their email matches a specific domain (e.g. "mattermost.org")',
                        })}{'. '}
                        <em>
                            This also allows you to restrict the team to users who have full CERN accounts
                            (no lightweight/social/guest accounts).
                        </em>
                    </span>
                }
                descriptionAboveContent={true}
                inputFieldData={{name: 'name'}}
                inputFieldValue={showAllowedDomains}
                handleChange={handleEnableAllowedDomains}
            />
            {showAllowedDomains &&
            <SelectTextInput
                id='allowedDomains'
                placeholder={formatMessage({id: 'general_tab.AllowedDomainsExample', defaultMessage: 'corp.mattermost.com, mattermost.com'})}
                aria-label={formatMessage({id: 'general_tab.allowedDomains.ariaLabel', defaultMessage: 'Allowed Domains'})}
                value={allowedDomains}
                onChange={handleOnChangeDomains}
                handleNewSelection={updateAllowedDomains}
                isClearable={false}
                description={
                    <>
                        {formatMessage({id: 'general_tab.AllowedDomainsTip', defaultMessage: 'Seperate multiple domains with a space, comma, tab or enter.'})}
                        <br />
                        <br />
                        <strong>
                            {hasCERNRestriction
                                ? <>
                                    {hasOnlyCERNRestriction
                                        ? <em>Restricted to CERN accounts (no lightweight/social/guest accounts).</em>
                                        : <em>
                                            Restricted to CERN accounts (no lightweight/social/guest accounts) and any
                                            accounts with an email matching one of the other specified domains.
                                        </em>
                                    }
                                </>
                                : <>
                                    Enter <a onClick={setCERNRestriction}><code>cern.ch</code></a> in order to restrict
                                    access to all CERN accounts; this will work regardless of the email address used,
                                    i.e. someone using their institute's email address (or any other email address)
                                    instead of a CERN email address will be able to join as well.
                                </>
                            }
                        </strong>
                    </>
                }
            />
            }
        </>
    );
};

export default AllowedDomainsSelect;
